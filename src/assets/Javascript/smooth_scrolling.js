(function ($) {
  "use strict"; // Start of use strict
  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, "easeInOutExpo");


        var displayType=detectDisplayType();
        console.log(displayType);
        if (displayType == "xs" || displayType=="sm") {
          // ----------------------------------------------------------
          // open or close navbar
          $('#sidebar, #content').toggleClass('active');
          // close dropdowns
          $('.collapse.in').toggleClass('in');
          // and also adjust aria-expanded attributes we use for the open/closed arrows
          // in our CSS
          $('a[aria-expanded=true]').attr('aria-expanded', 'false');
          // ----------------------------------------------------------
        }

        return false;
      }
    }
  });

  // // Closes responsive menu when a scroll trigger link is clicked
  // $('.js-scroll-trigger').click(function() {
  //   $('.navbar-collapse').collapse('hide');
  // });

  // Activate scrollspy to add active class to navbar items on scroll
  // $('body').scrollspy({
  //   target: '#sideNav'
  // });

})(jQuery); // End of use strict

function detectDisplayType() {
  if (screen.width < 768)
    return "xs";
  else if (screen.width < 992)
    return "sm";
  else if (screen.width < 1200)
    return "md";
  else
    return "lg";

}