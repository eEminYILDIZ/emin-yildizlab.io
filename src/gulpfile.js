const gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var concatCss = require('gulp-concat-css');
var clean = require('gulp-clean');

const outputDirecteries = {
    bundle: './assets/Bundles/',
    buildCss: './assets/Css/Build/',
};

const srcFiles = {
    jsFiles: [
        "./assets/Javascript/app.js",
        "./assets/Javascript/smooth_scrolling.js",
        "./assets/Javascript/jquery.mCustomScrollbar.concat.min.js"
    ],
    cssFiles: [
        "./assets/Css/Build/site.css",
        "./assets/Css/jquery.mCustomScrollbar.min.css"
    ],
    lessFiles: [
        "./assets/Css/Less/*.less"
    ]
};

const bundleFileNames = {
    cssFile: "bundle_156849.css",
    jsFile: "bundle_156429.js"
}

const directoryNamesForClean_Before =
    [
        outputDirecteries.bundle + "/**.*",
        outputDirecteries.buildCss + "/**.*",
        '../public/**.*',
    ];

const directoryNamesForClean_After =
    [
        outputDirecteries.bundle + "/**.*",
        outputDirecteries.buildCss + "/**.*",
    ];

const srcAssetFileNamesForCopy = [
    outputDirecteries.bundle + "/" + bundleFileNames.jsFile,
    outputDirecteries.bundle + "/" + bundleFileNames.cssFile,
    './bower_components/jquery/dist/jquery.min.js',
    './bower_components/bootstrap/dist/js/bootstrap.bundle.min.js',
	'./bower_components/bootstrap/dist/js/bootstrap.bundle.min.js.map',
    './bower_components/jquery.easing/js/jquery.easing.min.js',
    './bower_components/bootstrap/dist/css/bootstrap.min.css',
	'./bower_components/bootstrap/dist/css/bootstrap.min.css.map',
	'./assets/img/navicon.png',
	'./assets/img/_navLogo_.png',
    './assets/img/_profile_.png',
];

const srcHtmlFileNamesForCopy = [
    './index.html',
    './resume.html',

    './sitemap.xml',
    './robots.txt',
    './googlea5c9c6ccbed899a5.html',
    './yandex_d6ae91d578f92ef2.html',
];

const destHtmlDirectoryNameForCopy = '../public/';
const destAssetDirectoryNameForCopy = '../public/assets/';

gulp.task('clear_before_build', function () {
    return gulp.src(directoryNamesForClean_Before, { read: false })
        .pipe(clean({ force: true }));
});

gulp.task('clear_after_build', function () {
    return gulp.src(directoryNamesForClean_After, { read: false })
        .pipe(clean({ force: true }));
});

gulp.task('compile_less', function () {
    return gulp.src(srcFiles.lessFiles)
        .pipe(less())
        .pipe(gulp.dest(outputDirecteries.buildCss));
});

gulp.task('bundle_css', function () {
    return gulp.src(srcFiles.cssFiles)
        .pipe(concatCss(bundleFileNames.cssFile))
        .pipe(minifyCss())
        .pipe(gulp.dest(outputDirecteries.bundle));
});

gulp.task('bundle_js', function () {
    return gulp.src(srcFiles.jsFiles)
        .pipe(concat(bundleFileNames.jsFile))
        .pipe(uglify())
        .pipe(gulp.dest(outputDirecteries.bundle));
});

gulp.task('copy_assets', function () {
    return gulp.src(srcAssetFileNamesForCopy)
        .pipe(gulp.dest(destAssetDirectoryNameForCopy));
});

gulp.task('copy_htmls', function () {
    return gulp.src(srcHtmlFileNamesForCopy)
        .pipe(gulp.dest(destHtmlDirectoryNameForCopy));
});

gulp.task('default', gulp.series(
    'clear_before_build',
    'compile_less',
    'bundle_css',
    'bundle_js',
    'copy_assets',
    'copy_htmls',
    'clear_after_build',
));